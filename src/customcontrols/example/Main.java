package customcontrols.example;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Luca Bertolini on 07/02/2017.
 */
public class Main extends Application{

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        CustomControl in = new CustomControl(1);
        CustomControl out = new CustomControl(2);

        HBox hBox = new HBox(10, in, out);
        hBox.setPadding(new Insets(20));
        hBox.setAlignment(Pos.CENTER);
        primaryStage.setScene(new Scene(hBox));

        out.positionProperty().bind(in.positionProperty());
        //out.positionProperty().bindBidirectional(in.positionProperty()); //<----- using this line instead of the previous one it works

        //timer to simulate a program that changes the position of the element every 1000 millis
        Timer timer = new Timer(true);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> in.setPosition(Math.random()));
            }
        }, 0, 1000);


        primaryStage.setWidth(400);
        primaryStage.setHeight(600);
        primaryStage.show();
    }
}
