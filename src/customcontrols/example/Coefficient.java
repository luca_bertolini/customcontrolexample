package customcontrols.example;

/**
 * Created by Luca Bertolini on 22/01/2017.
 */
public class Coefficient {
    public          double          x;
    public          double          y;
    public          double          width;
    public          double          heigth;
    public          double          fontSize;
}
