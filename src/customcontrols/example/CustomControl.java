package customcontrols.example;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.DoublePropertyBase;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Created by Luca Bertolini on 07/02/2017.
 */
public class CustomControl extends Region {
    private static final double PREF_WIDTH = 36.137962;
    private static final double PREF_HEIGTH = 581.89221;
    private static final double RATIO = PREF_HEIGTH / PREF_WIDTH;
    private double currentWidth;
    private double currentHeight;
    private boolean loaded;
    private int number;

    private Pane root;
    private Pane background;
    private Region level;
    private Region mask;
    private Text num;

    private Coefficient levelCoefficient;
    private Coefficient maskCoefficient;
    private Coefficient numCoefficient;

    private DoubleProperty positionProperty;


    public CustomControl(int number) {
        this.number = number;

        positionProperty = new DoublePropertyBase(0.0) {
            @Override public Object getBean() { return CustomControl.this; }
            @Override public String getName() { return "position"; }
            @Override protected void invalidated() {
                //as you can see in the terminal this method is not called on control 2 using only bind()
                System.out.println("Control" + number + " positionProperty invalidated");
                updatePosition();
            }
        };

        //adding and styling elements
        root = new Pane();
        background = new Pane();
        level = new Region();
        mask = new Region();
        num = new Text(-8, 250, Integer.toString(number));

        if (Double.compare(getPrefWidth(), 0.0) <= 0 || Double.compare(getPrefHeight(), 0.0) <= 0 ||
                Double.compare(getWidth(), 0.0) <= 0 || Double.compare(getHeight(), 0.0) <= 0) {
            if (getPrefWidth() > 0 && getPrefHeight() > 0) {
                setPrefSize(getPrefWidth(), getPrefHeight());
            } else {
                setPrefSize(PREF_WIDTH, PREF_HEIGTH);
            }
        }

        background.setStyle("-fx-shape: \"m 0.0130615,0.00935185 33.8793435,0 0,545.52392815 -33.8793435,0 z\"; -fx-background-color: #5e5e5e;");
        level.setStyle("-fx-shape: \"m 1.2653175,34.066522 31.4285675,0 0,407.625678 -31.4285675,0 z\"; -fx-background-color: #6ee32f");
        mask.setStyle("-fx-shape: \"m 0.3079045,33.857592 33.3771605,0 0,407.834608 -33.3771605,0 z\"; -fx-background-color: #5e5e9f");
        num.setFont(new Font("sans-serif", 37.5));
        num.setFill(Color.WHITE);

        loaded = false;
        background.setCacheShape(false);
        background.setScaleShape(false);
        level.setCacheShape(false);
        level.setScaleShape(false);
        mask.setCacheShape(false);
        mask.setScaleShape(false);

        root.getChildren().addAll(background, level, mask, num);
        getChildren().setAll(root);

        widthProperty().addListener(observable -> resize());
        heightProperty().addListener(observable -> resize());
    }

    private void computeCoefficients() {
        //compute coefficients used for resize
        levelCoefficient = new Coefficient();
        levelCoefficient.x = (level.getBoundsInParent().getMinX() - background.getBoundsInParent().getMinX()) / background.getBoundsInParent().getWidth();
        levelCoefficient.y = (level.getBoundsInParent().getMinY() - background.getBoundsInParent().getMinY()) / background.getBoundsInParent().getHeight();
        levelCoefficient.width = level.getBoundsInParent().getWidth() / background.getBoundsInParent().getWidth();
        levelCoefficient.heigth = level.getBoundsInParent().getHeight() / background.getBoundsInParent().getHeight();

        maskCoefficient = new Coefficient();
        maskCoefficient.x = (mask.getBoundsInParent().getMinX() - background.getBoundsInParent().getMinX()) / background.getBoundsInParent().getWidth();
        maskCoefficient.y = (mask.getBoundsInParent().getMinY() - background.getBoundsInParent().getMinY()) / background.getBoundsInParent().getHeight();
        maskCoefficient.width = mask.getBoundsInParent().getWidth() / background.getBoundsInParent().getWidth();
        maskCoefficient.heigth = mask.getBoundsInParent().getHeight() / background.getBoundsInParent().getHeight();

        numCoefficient = new Coefficient();
        numCoefficient.x = (num.getX() - background.getBoundsInParent().getMinX()) / background.getBoundsInParent().getWidth();
        numCoefficient.y = (num.getY() - background.getBoundsInParent().getMinY()) / background.getBoundsInParent().getHeight();
        numCoefficient.fontSize = num.getFont().getSize() / background.getBoundsInParent().getHeight();

        background.setCacheShape(true);
        background.setScaleShape(true);
        level.setCacheShape(true);
        level.setScaleShape(true);
        mask.setCacheShape(true);
        mask.setScaleShape(true);

        loaded = true;
    }

    private void resize() {
        //resize the customControl
        if (!loaded) {
            computeCoefficients();
            return;
        }

        currentWidth  = getWidth() - getInsets().getLeft() - getInsets().getRight();
        currentHeight = getHeight() - getInsets().getTop() - getInsets().getBottom();

        if (RATIO * currentWidth > currentHeight) {
            currentWidth = 1 / (RATIO / currentHeight);
        } else if (1 / (RATIO / currentHeight) > currentWidth) {
            currentHeight = RATIO * currentWidth;
        }

        if (currentWidth > 0 && currentHeight > 0) {
            root.setMaxSize(currentWidth, currentHeight);
            root.setPrefSize(currentWidth, currentHeight);
            root.relocate((getWidth() - currentWidth) * 0.5, (getHeight() - currentHeight) * 0.5);

            background.setPrefSize(currentWidth, currentHeight);
            background.relocate(0, 0);

            level.setPrefSize(currentWidth * levelCoefficient.width, currentHeight * levelCoefficient.heigth);
            level.relocate(currentWidth * levelCoefficient.x, currentHeight * levelCoefficient.y);

            mask.setPrefSize(currentWidth * maskCoefficient.width, currentHeight * maskCoefficient.heigth);
            mask.relocate(currentWidth * maskCoefficient.x, currentHeight * maskCoefficient.y);

            String family = num.getFont().getFamily();
            num.setFont(new Font(family, currentHeight * numCoefficient.fontSize));
            num.setX(currentWidth * numCoefficient.x);
            num.setY(currentHeight * numCoefficient.y);
        }
    }


    public DoubleProperty positionProperty() { return positionProperty; }
    public double getPosition() { return positionProperty.get(); }
    public void setPosition(double value) { positionProperty.set(value); }


    private void updatePosition(){
        //update the position of the "mask" element to uncover the "level" element
        if (!loaded) return;
        double position = 1.0 - getPosition();
        mask.setPrefSize(currentWidth * maskCoefficient.width, currentHeight * maskCoefficient.heigth * position);
    }
}
